<!DOCTYPE HTML>
<html>
	<head>
		<title> Create a new Story </title>
	</head>
	
	<body><div>
	<?php
		session_start();
		// check if valid user is editting
		if (isset($_POST['title']) ) {
			$title = htmlentities($_POST['title']);
			$content = htmlentities($_POST['content']);
			if ($title == "") {
				echo "Title field is empty!";
			} 
			else {	
				require ('connect.php');
				
				$stmt = $mysqli->prepare("insert into story (user, title, content) values (?, ?, ?)");
				if(!$stmt){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$stmt->bind_param('sss', $_SESSION['name'], $title, $story);
				$stmt->execute();
				$stmt->close();
				header("Location: main.php");
				exit;
			} 
		}
	?>
	
	
	<!--form that allows user to create a new story-->
	
	<form action="new.php" method="POST">

	<p>
        <label>Title:</label><br>
		<textarea name="title" rows=1></textarea>
    </p>
    <p>
		<label>Content:</label><br>
		<textarea name="content" rows=10></textarea>
	</p>
	<p>
		<input type="submit" name = "submit" value = "submit" />
    </p>
	</form>
	</div>
	</body>
</html>